export class Summary {
    public amountNew: number;
    public amountResolved: number;
    public amountApproved: number;
    public amountDisapproved: number;
    public amountAssign: number;
    public amountClosed: number;
}
